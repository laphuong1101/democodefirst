﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DemoCodeFirst.Models;

namespace DemoCodeFirst.Controllers
{
    public class StudentsController : Controller
    {
        private ManagerStudentContext db;
        public StudentsController()
        {
            if (db == null)
            {
                db = new ManagerStudentContext();
            }
        }
        // GET: Students
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListStudent(String KeySearch, int CurrentPage)
        {
            try
            {
                var list = db.Students.ToList();
             //   var list = db.Students.Where(p => p.FullName.Contains(KeySearch)).ToList());
                var TotalPage = list.Count() % 4 == 0 ? list.Count() / 4 : (list.Count() / 4) + 1;
                var result = list.Skip((CurrentPage - 1) * 4).Take(4).ToList();
                return Json(new {code = 200, TotalPage = TotalPage, list = result, msg = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new {code = 500, msg = "Get List Student Failed" }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult Add(string StudentId, string FullName, DateTime BirthDay)
        {
            try
            {
                Student student = new Student()
                {
                    StudentId = StudentId,
                    FullName = FullName,
                    BirthDay = BirthDay
                };
                db.Students.Add(student);
                db.SaveChanges();
                var TotalPage = db.Students.Count() % 4 == 0 ? db.Students.Count() / 4 : (db.Students.Count() / 4) + 1;
                return Json(new { code = 200, TotalPage = TotalPage, msg = "Add Success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = 500, msg = "Add Faild" + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            try
            {
                var student = db.Students.Find(id);
                db.Students.Remove(student);
                db.SaveChanges();
                var TotalPage = db.Students.Count() % 4 == 0 ? db.Students.Count() / 4 : (db.Students.Count() / 4) + 1;
                return Json(new { code = 200, TotalPage = TotalPage, msg = "Record delete success" }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(new { code = 500, msg = "Record is invalid or deleted :" + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteAll(string[] selectedID)
        {
            try
            {
                foreach (string id in selectedID)
                {
                    Student obj = db.Students.Find(id);
                    db.Students.Remove(obj);
                }
                db.SaveChanges();

                var TotalPage = db.Students.Count() % 4 == 0 ? db.Students.Count() / 4 : (db.Students.Count() / 4) + 1;
                return Json(new { code = 200, TotalPage = TotalPage, msg = "All the customers deleted successfully!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = 500, msg = "Deleted failed! - " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Edit(string id, string FullName, DateTime BirthDay)
        {
            try
            {
                var query = db.Students.Single(s => s.StudentId.Equals(id));


                query.FullName = FullName;
                query.BirthDay = BirthDay;
                db.SaveChanges();
                return Json(" Edited successfully!");
            }
            catch (Exception ex)
            {
                return Json("Editted failed! - " + ex.Message);
            }
        }
    }
}






