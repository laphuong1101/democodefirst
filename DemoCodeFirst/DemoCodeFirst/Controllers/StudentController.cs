﻿using DemoCodeFirst.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PagedList;

namespace DemoCodeFirst.Controllers
{
    public class StudentController : Controller
    {
        private ManagerStudentContext db;
        public StudentController()
        {
            if (db == null)
            {
                db = new ManagerStudentContext();
            }
        }
        // GET: Student
        public ActionResult Index(int page = 1, int pagesize = 1)
        {

            return View(db.Students.ToList().ToPagedList(page,pagesize));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Student student)
        {
            db.Students.Add(student);
            db.SaveChanges();
            return Redirect("Index");
        }

        [HttpPost]
        public void Delete(string id)
        {
            var query = db.Students.Single(s => s.StudentId.Equals(id));
            db.Students.Remove(query);
            db.SaveChanges();
        }

        [HttpPost]
        public ActionResult DeleteAll(string[] selectedID)
        {

            foreach (string id in selectedID)
            {
                Student obj = db.Students.Find(id);
                db.Students.Remove(obj);
            }
            db.SaveChanges();
            return Json("All the customers deleted successfully!");
        }

        [HttpPost]
        public ActionResult Add(string Id, string FullName, DateTime BirthDay)

        {
            Student student = new Student() {
                StudentId = Id,
                FullName = FullName,
                BirthDay = BirthDay
            };

            db.Students.Add(student);
            db.SaveChanges();
            return PartialView("PartialViewSave", student);
        }

        [HttpPost]
        public void Edit(string id, string FullName, DateTime BirthDay)
        {
            var query = db.Students.Single(s => s.StudentId.Equals(id));


            query.FullName = FullName;
            query.BirthDay = BirthDay;
            db.SaveChanges();
        }


    }
}