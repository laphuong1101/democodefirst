﻿namespace DemoCodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Classes",
                c => new
                    {
                        ClassId = c.String(nullable: false, maxLength: 10),
                        ClassName = c.String(),
                    })
                .PrimaryKey(t => t.ClassId);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        StudentId = c.String(nullable: false, maxLength: 10),
                        FullName = c.String(nullable: false, maxLength: 50),
                        BirthDay = c.DateTime(nullable: false),
                        Gender = c.Boolean(nullable: false),
                        CodeClass = c.String(),
                        Class_ClassId = c.String(maxLength: 10),
                    })
                .PrimaryKey(t => t.StudentId)
                .ForeignKey("dbo.Classes", t => t.Class_ClassId)
                .Index(t => t.Class_ClassId);
            
            CreateTable(
                "dbo.Marks",
                c => new
                    {
                        StudentId = c.String(nullable: false, maxLength: 10),
                        SubjectId = c.String(nullable: false, maxLength: 10),
                        MarkStudent = c.Single(nullable: false),
                    })
                .PrimaryKey(t => new { t.StudentId, t.SubjectId })
                .ForeignKey("dbo.Students", t => t.StudentId, cascadeDelete: true)
                .ForeignKey("dbo.Subjects", t => t.SubjectId, cascadeDelete: true)
                .Index(t => t.StudentId)
                .Index(t => t.SubjectId);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        SubjectId = c.String(nullable: false, maxLength: 10),
                        SubjectName = c.String(nullable: false, maxLength: 10),
                    })
                .PrimaryKey(t => t.SubjectId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Marks", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.Marks", "StudentId", "dbo.Students");
            DropForeignKey("dbo.Students", "Class_ClassId", "dbo.Classes");
            DropIndex("dbo.Marks", new[] { "SubjectId" });
            DropIndex("dbo.Marks", new[] { "StudentId" });
            DropIndex("dbo.Students", new[] { "Class_ClassId" });
            DropTable("dbo.Subjects");
            DropTable("dbo.Marks");
            DropTable("dbo.Students");
            DropTable("dbo.Classes");
        }
    }
}
