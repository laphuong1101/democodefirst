﻿namespace DemoCodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Students", name: "Class_ClassId", newName: "ClassId");
            RenameIndex(table: "dbo.Students", name: "IX_Class_ClassId", newName: "IX_ClassId");
            DropColumn("dbo.Students", "CodeClass");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Students", "CodeClass", c => c.String());
            RenameIndex(table: "dbo.Students", name: "IX_ClassId", newName: "IX_Class_ClassId");
            RenameColumn(table: "dbo.Students", name: "ClassId", newName: "Class_ClassId");
        }
    }
}
