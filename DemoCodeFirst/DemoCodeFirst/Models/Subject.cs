﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoCodeFirst.Models
{
    public class Subject
    {
        [Key]
        [StringLength(10)]
        public string SubjectId { get; set; }

        [StringLength(10)]
        [Required]
        public string SubjectName { get; set; }

        public virtual ICollection<Mark> Marks { get; set; }
    }
}