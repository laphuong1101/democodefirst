﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DemoCodeFirst.Models
{
    public class Mark
    {
        [Key]
        [Column(Order =1)]
        public string StudentId { get; set; }

        [Key]
        [Column(Order = 2)]
        public string SubjectId { get; set; }

        public float MarkStudent { get; set; }

        public virtual Student Student { get; set; }

        public virtual Subject Subject { get; set; } 
    }
}