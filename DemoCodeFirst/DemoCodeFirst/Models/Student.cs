﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoCodeFirst.Models
{
    public class Student
    {
        [Key]
        [StringLength(10)]
        [DisplayName("Student ID")]
        [Required]
        public string StudentId { get; set; }

        [StringLength(50)]
        [Required]
        [DisplayName("Full Name")]
        public string FullName { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true,DataFormatString = "{0:dd/MM/yyyy}")]
        [DisplayName("Birth Day")]
        public DateTime? BirthDay { get; set; }
        public bool Gender { get; set; }

        public string ClassId { get; set; }

        public virtual Class Class { get; set; }

        public virtual ICollection<Mark> Marks { get; set; }
    }
}