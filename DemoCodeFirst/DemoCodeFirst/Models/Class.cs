﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoCodeFirst.Models
{
    public class Class
    {
        [Key]
        [StringLength(10)]
        public string ClassId { get; set; }

        public string ClassName { get; set; }

        public virtual ICollection<Student> Students { get; set; }
    }
}